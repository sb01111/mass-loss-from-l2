#Max equipotential calculator
#6427319
#Last updated 26/05/2020
#This code calculates the co-ordinates of the L2 equipotential and exports it to a file
import matplotlib.pylab as plt
import numpy as np
from sympy.solvers import solve
from sympy import Float
from sympy import Symbol
import sympy as sym

def L_point_solver(mu): #Finds the L2 and L3 points of the system
    x=Symbol('x')  
    sol_L2 = solve(x-mu/((x-1+mu)**2) - (1-mu)/((x+mu)**2),x)
    sol_L3 = solve(x-mu1/((x-1+mu1)**2) - (1-mu1)/((x+mu1)**2),x)
    return  sol_L2[0], sol_L3[0]

#Finds the element of the array that is nearest the desired value
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    for count, element in enumerate(array):
        if element == array[idx]:
            index = count
    return array[idx], index

#Calculates x and y from polar co-ordinates, as well as the potential at each point
def potential_solver(r, th, phis):
    x=r*np.cos(th)
    y=r*np.sin(th)
    phi=(-mu/((x-1+mu)**2 +y**2)**0.5 - (1-mu)/((x+mu)**2 + y**2)**0.5 - 0.5*(x**2 +y**2))
    np.savetxt(f'equi_test.dat', np.transpose([phi, x, y]))
    for count1, i in enumerate(phi):
        value = phis
        phiv, index = find_nearest(phi,value)
        xvals.append(x[index])
        yvals.append(y[index])
        phival.append(phiv)
    return x, y

#Appends x and y value to a file, which can be imported into mass_loss_from_L2_master.py
def loop(q, L2, phis):
    for theta in th:
        lower = float(L2)-0.001
        upper = 2*float(L2)
        r=np.linspace(lower,upper,200)
        x, y = potential_solver(r,theta, phis)
    np.savetxt(f'equi{int(100*q)}.dat', np.transpose([phival, xvals, yvals]))

#q values relevant to my report, with the corresponding potential at L2
qvals= [0.001, 0.02, 0.4, 0.5, 0.6, 1.0] 
phis= [-1.519297219836738, -1.6116905413091658, -1.7794843211901967, -1.7737406767952604, -1.765551463151703, -1.7283981845479832]
th = np.linspace(0, 2*np.pi, 3600)
for count, q in enumerate(qvals):
    M2 = 1
    M1 = M2/q
    mu = M2/(M1+M2)
    mu1=M1/(M1+M2)
    a=(mu1+mu)
    xvals=[]
    yvals=[]
    phival=[]
    L2, L3 = L_point_solver(mu)
    loop(q, L2, phis[count])
    xvals=[]
    yvals=[]
    phival=[]


