import numpy as np 
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
import shapely.geometry as geo 


#Star masses
M1, M2, a = 2, 1.02, 2
mu = M2/(M1+M2)
q = M2/M1
AU=1.5E11
M1_b = a*mu
M2_b = a - M1_b

print("mu is",mu,", q is", q, ".")

#Functions defining the potential, energy and the angular momentum of a particle.
def trajvalues():
    ph=-mu/((x-1+mu)**2 +y**2)**0.5 - (1-mu)/((x+mu)**2 + y**2)**0.5 - 0.5*(x**2 +y**2)
    E=ph + 0.5*(vx**2 + vy**2) + x**2 + y**2 + x*vy + y*vx
    J=x**2 + y**2 + x*vy + y*vx
    return ph, E, J

#Function that takes values of the roche lobe for each star, from the UoC roche lobe calculator
#and plots them to determine a polygon in which the ejected particle can't enter
def plot_rochelobe2():
    rvals1, xvals1, rvals2, xvals2=[], [], [], []
    r1, r2 = np.loadtxt("rochelobe1_02Output.txt"), np.loadtxt("rochelobe2_02Output.txt")
    th, th2 = np.linspace(-np.pi-np.pi/2,np.pi-np.pi/2,361), np.linspace(-np.pi+np.pi/2,np.pi+np.pi/2,361)
    
    for count, i in enumerate(th):  
        xr1 = r1[count]*a*np.cos(i) - M1_b
        yr1 = r1[count]*a*np.sin(i)
        rvals1.append([xr1, yr1])
        xvals1.append(xr1)

    for count, i in enumerate(th2):  
        xr2 = r2[count]*a*np.cos(i) + M2_b
        yr2 = r2[count]*a*np.sin(i)
        rvals2.append([xr2, yr2])
        xvals2.append(xr2)
    rvals1, rvals2 =np.array(rvals1), np.array(rvals2)
    poly1, poly2 = geo.Polygon(rvals1), geo.Polygon(rvals2)
    x1, y1 = poly1.exterior.xy
    x2, y2 = poly2.exterior.xy
    plt.plot(x1,y1, color='0.7')
    plt.plot(x2,y2, color='0.7')
    L2, L3 = max(xvals2), min(xvals1)
    return poly1, poly2, L2, L3

poly1, poly2, L2, L3 = plot_rochelobe2()

#Function for defining equations of motion and returning them in the form of 4 ODEs
def EOM(t, E):
    x = E[0]
    y = E[1]
    vx = E[2]
    vy = E[3]
    dxdt = vx 
    dydt = vy
    dvxdt = -((1-mu)*(x+mu)/((x+mu)**2 + y**2)**1.5 + mu*(x+mu-1)/((x+mu-1)**2 + y**2)**1.5 - x) +2*vy 
    dvydt = -((1-mu)*y/((x+mu)**2 +y**2)**1.5 + mu*y/((x+mu-1)**2 +y**2)**1.5 -y) -2*vx 
    return dxdt,dydt,dvxdt,dvydt

f=open('solved2.dat','ab')

def histplotreal():
    X=np.random.rand(30)
    Y=np.random.rand(30)
    sigma=np.std(X)
    s=sigma*np.sqrt(-2*np.log(1-X))
    th=2*np.pi*Y
    u=s*np.cos(th)
    v=s*np.sin(th)
    #plt.hist(u, bins=100)
    #plt.hist(v, bins=100)
    #plt.xlabel("x")
    #plt.ylabel("y")
    #plt.show()
    #plt.hist(s, bins=100)
    #plt.hist(0.99*np.sqrt(u**2+v**2), bins=100)
    #plt.show()
    return u, v, s
u, v, s = histplotreal()

def eliminate_neg_u(u, v):
    utemp = u.tolist()
    vtemp = v.tolist()
    print(utemp[-1])
    print(len(utemp))
    for i in range(1,10):
        for count, i in enumerate(utemp):
            if i < 0.0:
                utemp.pop(count)
                vtemp.pop(count)
        print(utemp[-1], len(utemp), len(vtemp))
    return utemp, vtemp
u, v = eliminate_neg_u(u,v)
u, v, = np.array(u), np.array(v)

for count, i in enumerate(u):
    #Setting initial conditions, with possibilty of varying initial x and y offset
    xoffset = 0.0 
    yoffset = 0.0
    xinit, yinit = L2 +xoffset, yoffset

    #Initial conditions for x, y, vx, vy
    E0 = [xinit, yinit, i, v[count]]
    tspan=np.linspace(0,15.0,10000)

    #Solving ODEs to give values for x, y, vx and vy for the particle.
    sol = solve_ivp(EOM, 
                    [tspan[0], tspan[-1]], E0, t_eval=tspan, atol=1E-12, rtol=1E-12)
    x, y, vx, vy = sol.y

#This function checks whether or not the x values of the particle's trajectory are outside the bounds
#of the roche surfaces and stops the trajectory if it is.
    def rochecheck():
        xnew=[]
        ynew=[]
        vxnew=[]
        vynew=[]
        for count, xi in enumerate(x):
            if poly1.contains(geo.Point(xi,y[count])) == False and poly2.contains(geo.Point(xi,y[count])) == False:
                xnew.append(xi)
                ynew.append(y[count])
                vxnew.append(vx[count])
                vynew.append(vy[count])
            else:
                break
        xn=np.array(xnew)
        yn=np.array(ynew)
        vxn=np.array(vxnew)
        vyn=np.array(vynew)
        return xn, yn, vxn, vyn
    x, y, vx, vy = rochecheck()
    print(len(x))
#Calling additional variables
    ph, E, J = trajvalues()
    np.savetxt(f, np.transpose([x, y]))
#Jacobi constant to check if the difference between energy and angular momentum stays constant.
    C=E-J
    plt.plot(x,y, linewidth=0.5, color='C0')
#Exporting data
#np.savetxt("solved.dat", np.transpose([x, y, vx, vy, E, J, C]), header = "x \
#         y      vx      vy      E,      J,     C", fmt='%.5f')

#Code for labelling second and third Lagrange points as well as the positions of each star.
labels = ['L2', 'L3', 'S1', 'S2']
xs = [L2 , L3, -M1_b, M2_b]
ys = [0,0,0,0,0]
for i, label in enumerate(labels):
    xval=xs[i]
    yval=ys[i]
    plt.scatter(xval,yval,marker = 'x', color = 'red', s=4.0) 
    plt.text(xval+0.02*a, yval,label,fontsize=8)

plt.title("x against y, q={0:.3}, (x,y)=({1:.2f},{2:.2f}), (vx,vy) = (range between 0 and 1)".format(q, E0[0]-L2, E0[1]) )
plt.xlabel('x distance/ AU')
plt.ylabel('y distance/ AU')
#plt.plot(x,y, linewidth=1.0, color='C0')
plt.Axes.get_xscale
plt.savefig(f"TP {q}, ({E0[0]},{E0[1]}), ({E0[2]},{E0[3]}).png", dpi=300)
plt.show()
