import numpy as np 
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
import shapely.geometry as geo 
from sympy.solvers import solve
from sympy import Float
from sympy import Symbol

#Star masses

def L2_solver():
    x=Symbol('x')  
    sol = solve(x-mu/((x-1+mu)**2) - (1-mu)/((x+mu)**2),x)
    L2t = sol[0]
    return  L2t

def L3_solver():
    x=Symbol('x')  
    sol = solve(x-mu1/((x-1+mu1)**2) - (1-mu1)/((x+mu1)**2),x)
    L3t = sol[0]
    return  L3t

#Functions defining the potential, energy and the angular momentum of a particle.
def trajvalues(x, y, vx, vy):
    ph=-mu/((x-1+mu)**2 +y**2)**0.5 - (1-mu)/((x+mu)**2 + y**2)**0.5 - 0.5*(x**2 +y**2)
    E=ph + 0.5*(vx**2 + vy**2) + x**2 + y**2 + x*vy + y*vx
    J=x**2 + y**2 + x*vy + y*vx
    return ph, E, J

#Function that takes values of the roche lobe for each star, from the UoC roche lobe calculator
#and plots them to determine a polygon in which the ejected particle can't enter
def plot_rochelobe2(i):
    rvals1, xvals1, rvals2, xvals2=[], [], [], []
    r1, r2 = np.loadtxt(f"star1_{int(i*100)}Output.txt"), np.loadtxt(f"star2_{int(i*100)}Output.txt")
    th, th2 = np.linspace(-np.pi-np.pi/2,np.pi-np.pi/2,361), np.linspace(-np.pi+np.pi/2,np.pi+np.pi/2,361)
    for count, i in enumerate(th):  
        xr1 = r1[count]*a*np.cos(i) + M1_b
        yr1 = r1[count]*a*np.sin(i)
        rvals1.append([xr1, yr1])
        xvals1.append(xr1)

    for count, i in enumerate(th2):  
        xr2 = r2[count]*a*np.cos(i) + M2_b
        yr2 = r2[count]*a*np.sin(i)
        rvals2.append([xr2, yr2])
        xvals2.append(xr2)
    rvals1, rvals2 =np.array(rvals1), np.array(rvals2)
    poly1, poly2 = geo.Polygon(rvals1), geo.Polygon(rvals2)
    x1, y1 = poly1.exterior.xy
    x2, y2 = poly2.exterior.xy
    plt.plot(x1,y1, color='0.7', linewidth=0.5)
    #plt.fill(x1,y1, color='0.7')
    #plt.fill(x2,y2, color='0.7')
    plt.plot(x2,y2, color='0.7', linewidth=0.5)
    L2, L3 = max(xvals2), min(xvals1)
    return poly1, poly2, L2, L3




#Function for defining equations of motion and returning them in the form of 4 ODEs
def EOM(t, E):
    x = E[0]
    y = E[1]
    vx = E[2]
    vy = E[3]
    dxdt = vx 
    dydt = vy 
    dvxdt = -((1-mu)*(x+mu)/((x+mu)**2 + y**2)**1.5 + mu*(x+mu-1)/((x+mu-1)**2 + y**2)**1.5 - x) +2*vy 
    dvydt = -((1-mu)*y/((x+mu)**2 +y**2)**1.5 + mu*y/((x+mu-1)**2 +y**2)**1.5 -y) -2*vx
    return dxdt,dydt,dvxdt,dvydt



#This function checks whether or not the x values of the particle's trajectory are outside the bounds
#of the roche surfaces and stops the trajectory if it is.
def rochecheck(x, y, vx, vy, poly1, poly2):
    xnew=[]
    ynew=[]
    vxnew=[]
    vynew=[]
    xn=[]
    yn=[]
    vxn=[]
    vyn=[]
    for count, xi in enumerate(x):
        if poly1.contains(geo.Point(xi,y[count])) == False and poly2.contains(geo.Point(xi,y[count])) == False:
            xnew.append(xi)
            ynew.append(y[count])
            vxnew.append(vx[count])
            vynew.append(vy[count])
        else:
            break
    xn=np.array(xnew)
    yn=np.array(ynew)
    vxn=np.array(vxnew)
    vyn=np.array(vynew)
    return xn, yn, vxn, vyn



def run_code(i, vx, vy):
    print(M1_b, M2_b, a)
    L2 = L2_solver()
    L2*=a
    L3 = L3_solver()
    L3*=-a
    poly1, poly2, L2t, L3t = plot_rochelobe2(i)
    #Setting initial conditions, with possibilty of varying initial x and y offset
    xoffset = 1E-10
    yoffset = 0.0
    xinit, yinit = L2 +xoffset, yoffset

    #Initial conditions for x, y, vx, vy
    E0 = [xinit, yinit, vx, vy]
    tspan=np.linspace(0,15.0,5000)

#Solving ODEs to give values for x, y, vx and vy for the particle.
    sol = solve_ivp(EOM, 
                    [tspan[0], tspan[-1]], E0, t_eval=tspan, atol=1E-12, rtol=1E-12)
    x, y, vx, vy = sol.y
    sol=0.0
    x, y, vx, vy = rochecheck(x, y, vx, vy, poly1, poly2)

    #Calling additional variables
    ph, E, J = trajvalues(x, y, vx, vy)
    #x, y, vx, vy = rochecheck()
    #Jacobi constant to check if the difference between energy and angular momentum stays constant.
    C=E-J
    #Exporting data
    print("Length of x is", len(x))
    np.savetxt("solved.dat", np.transpose([x, y, vx, vy, E, J, C]), header = "x \
        y      vx      vy      E,      J,     C", fmt='%.5f')
    plt.plot(x,y, linewidth=0.3, color='C0')
    #plt.scatter(x,y, s=1.0)
    #Code for labelling second and third Lagrange points as well as the positions of each star.
    labels = ['L2', 'L3', 'S1', 'S2', 'B']
    xs = [L2 , L3, M1_b, M2_b, 0]
    ys = [0,0,0,0,0]
    for i, label in enumerate(labels):
        xval=xs[i]
        yval=ys[i]
        plt.scatter(xval,yval,marker = 'x', color = 'red', s=2.0) 
        plt.text(xval+0.02*a, yval,label,fontsize=8)

    L2correction = xinit-Float(L2)
    plt.title("y against x, q={0:.3}, (x,y)=({1:.2f},{2:.2f}), ".format(q, float(L2correction), E0[1]) + "($v_{x}$,$v_{y}$)" + " = ({0:.2f},{1:.2f})".format(E0[2], E0[3]) )
    plt.xlabel('x/a')
    plt.ylabel('y/a')
    plt.ylim(-1.5,1.5)
    plt.xlim(-1.5,1.5)
    #plt.plot(x,y, linewidth=1.0, color='C0')
    plt.Axes.get_xscale
    plt.savefig(f"TP {q}, ({float(L2correction)},{E0[1]}), ({E0[2]},{E0[3]}).png", dpi=300)
    plt.show()
    #plt.close()
    x, y, vx, vy = 0.0, 0.0, 0.0, 0.0

r_t=0
vy_vals = [0.0] #np.linspace(-1,1,2)
for vy in vy_vals:
    vx_vals= [0.0] #np.linspace(-0.4,1,8)
    for vx in vx_vals:
        q_vals=[0.5]#np.linspace(0.02, 0.98,17)
        for i in q_vals:
            M2 = 1
            M1 = M2/i
            q=i
            mu = M2/(M1+M2)
            mu2=mu
            mu1=M1/(M1+M2)
            AU=1.5E11
            M1_b = -mu2
            M2_b = mu1
            a=(mu1+mu2)
            run_code(i, vx, vy)
            r_t += 1
            print("Just finished simulation", r_t, "out of", len(vy_vals)*len(vx_vals)*len(q_vals))
