mass_loss_from_L2.py contains the master code for the simulation.
equipotential.py contains the code for calculating the L2 equipotential.

mass_loss_from_L2.py reads in files from the equipotential folder and the 
roche_data folder in order to plot equipotentials and Roche lobes, so to
fully use the code, both of these folders will need to be downloaded too.

The Roche lobe calculator was downloaded from 
https://comp-astrophys-cosmol.springeropen.com/articles/10.1186/s40668-015-0008-8
There's a link to the page to download at the bottom of this paper in 
Availability and requirements.

The Archive folder contains previous versions of the code that aren't physically
accurate so shouldn't really be used. 