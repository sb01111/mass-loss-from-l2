#Mass Loss from L2
#6427319
#Last updated 26/05/2020
#This code simulates the trajectory of a particle ejected from the second Lagrangian point
#of a binary star system and plots a graph of the trajectory.

import numpy as np 
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
import shapely.geometry as geo 
from sympy.solvers import solve
from sympy import Float
from sympy import Symbol
import matplotlib 
 
def L_point_solver(): #Finds the L2 and L3 points of the system
    x=Symbol('x')  
    sol_L2 = solve(x-mu/((x-1+mu)**2) - (1-mu)/((x+mu)**2),x)
    sol_L3 = solve(x-mu1/((x-1+mu1)**2) - (1-mu1)/((x+mu1)**2),x)
    return  sol_L2[0], sol_L3[0]

#Function to find the Roche potential, energy, angular momentum, Jacobi Constant and
#Distance from binary barycentre for each set of x,y co-ordinates
def trajvalues(x, y, vx, vy): 
    ph=-mu/((x-1+mu)**2 +y**2)**0.5 - (1-mu)/((x+mu)**2 + y**2)**0.5 - 0.5*(x**2 +y**2)
    E=ph + 0.5*(vx**2 + vy**2) + x**2 + y**2 + x*vy - y*vx
    J=x**2 + y**2 + x*vy - y*vx
    C=E-J
    r=np.sqrt(x**2 + y**2)
    return ph, E, J, C, r

#Function that takes values of the roche lobe for each star, from the UoC roche lobe calculator
#and plots them to determine a polygon in which the ejected particle can't enter
def plot_rochelobe(i):
    rvals1, xvals1, rvals2, xvals2=[], [], [], []
    #Reading in radius and theta values from the Roche data file of the corresponding q value
    r1, r2 = np.loadtxt(f"roche_data/star1_{int(q*100)}Output.txt"), np.loadtxt(f"roche_data/star2_{int(q*100)}Output.txt")
    th, th2 = np.linspace(-np.pi-np.pi/2,np.pi-np.pi/2,361), np.linspace(-np.pi+np.pi/2,np.pi+np.pi/2,361)

    #Translating polar co-ordinates into Cartesian
    for count, i in enumerate(th):  
        xr1 = r1[count]*a*np.cos(i) - mu
        yr1 = r1[count]*a*np.sin(i)
        rvals1.append([xr1, yr1])
        xvals1.append(xr1)

    for count, i in enumerate(th2):  
        xr2 = r2[count]*a*np.cos(i) + mu1
        yr2 = r2[count]*a*np.sin(i)
        rvals2.append([xr2, yr2])
        xvals2.append(xr2)
    rvals1, rvals2 =np.array(rvals1), np.array(rvals2)
    poly1, poly2 = geo.Polygon(rvals1), geo.Polygon(rvals2)
    x1, y1 = poly1.exterior.xy          #Defining the Roche lobe polygon
    x2, y2 = poly2.exterior.xy
    plt.plot(x1,y1, color='0.7', linewidth=0.8)
    #plt.fill(x1,y1, color='0.7')           #Uncomment these lines to fill in the Roche lobe
    #plt.fill(x2,y2, color='0.7')
    plt.plot(x2,y2, color='0.7', linewidth=0.8)
    return poly1, poly2

def equipotential(q):
    phitemp, x, y = np.loadtxt(f"equipotentials/equi{int(q*100)}.dat", unpack=True)
    plt.plot(x, y ,linestyle = 'dashed', linewidth='0.8', color='0.5')

#Function for defining equations of motion and returning them in the form of 4 ODEs
def EOM(t, E):
    x = E[0]
    y = E[1]
    vx = E[2]
    vy = E[3]
    dxdt = vx 
    dydt = vy 
    dvxdt = -((1-mu)*(x+mu)/((x+mu)**2 + y**2)**1.5 + mu*(x+mu-1)/((x+mu-1)**2 + y**2)**1.5 - x) +2*vy 
    dvydt = -((1-mu)*y/((x+mu)**2 +y**2)**1.5 + mu*y/((x+mu-1)**2 +y**2)**1.5 -y) -2*vx
    return dxdt,dydt,dvxdt,dvydt

def on_segment(p, q, r):
#check if r lies on (p,q)
    if r[0] <= max(p[0], q[0]) and r[0] >= min(p[0], q[0]) and r[1] <= max(p[1], q[1]) and r[1] >= min(p[1], q[1]):
        return True
    return False

def orient(p, q, r):
    val = ((q[1] - p[1]) * (r[0] - q[0])) - ((q[0] - p[0]) * (r[1] - q[1]))
    if val == 0 : return 0
    return 1 if val > 0 else -1

#This function checks whether or not the x values of the particle's trajectory are outside the bounds
#of the roche surfaces and stops the trajectory if it is
def intersects(x, y, vx, vy, poly1, poly2):
    points=[]
    segments=[]
    for count, i in enumerate(x):
            points.append([i,y[count]])
    #Now we have an array of points 
    points2=points.copy()
    del points2[0]
    for count, i in enumerate(points):
        if count< len(points)-1:
           segment=((i[0],i[1]),(points2[count][0],points2[count][1]))
           segments.append(segment)
    #Now we have an array of 2 by 1 arrays which each contain point 1 and then the next point,
    #This is therefore an array of segments in the form: array = [P0, P1], [P1, P2], [P2, P3] etc. 
    index=0
    for count, i in enumerate(segments):
        if index != 0:
            break
        comparison_segment = i
        cp1, cp2 = comparison_segment
        if poly1.contains(geo.Point(cp1[0],cp1[1])) == True or poly2.contains(geo.Point(cp1[0],cp1[1])) == True:
           index = count +1     #Comment these three lines (105-107) to remove the check that 
           break                #determines whether the particle has entered the Roche lobe

        #Sets the comparison point, as we move through the particle's trajectory this segment is compared to 
        #all segments in the whole trajectory which is what p1 and p2 make up in the next for loop
        for count2, j in enumerate(segments[0:count+1]):
            if count2 == len(segments)-1:
                break
            current_segment = j
            p1, p2 = current_segment
            o1, o2, o3, o4 = orient(cp1, cp2, p1), orient(cp1, cp2, p2), orient(p1, p2, cp1), orient(p1, p2, cp2)
            if o1 != o2 and o3 != o4:
                if cp1[0] - p2[0] != 0 and cp1[1] - p2[1] !=0 and cp2[0] - p1[0] != 0 and cp2[1] - p1[0] !=0:
                    index = count+2
                    break 
    if index != 0: #(If the code stopped early)
        x, y, vx, vy =x.tolist(), y.tolist(), vx.tolist(), vy.tolist()
        del x[index:], y[index:], vx[index:], vy[index:]    #Removing everything past the index at which the code stopped
        x, y, vx, vy=np.array(x), np.array(y), np.array(vx), np.array(vy)
        return x, y, vx, vy
    else: 
        return x, y, vx, vy

def run_code(i):
    #Finding Lagrangian points
    L2, L3 = L_point_solver()
    L3 *= -1.0
    print(L2)

    #Setting initial conditions
    xoffset = 1E-10
    yoffset = 0.0
    vx = 0.0
    vy = 0.0
    E0 = [L2 + xoffset, yoffset, vx, vy]
    tspan=np.linspace(0,300,10000)

    #Solving ODEs to give values for x, y, vx and vy for the particle.
    sol = solve_ivp(EOM, 
                    [tspan[0], tspan[-1]], E0, t_eval=tspan, atol=1E-12, rtol=1E-12)
    x, y, vx, vy = sol.y

    poly1, poly2 = plot_rochelobe(i)    #Calculating and plotting Roche lobe
    equipotential(q)                    #Calculating and plotting max equipotential 
    #x, y, vx, vy = intersects(x, y, vx, vy, poly1, poly2)   #Applying limiting conditions to trajectory

    #Calling additional variables
    ph, E, J, C, r = trajvalues(x, y, vx, vy)
    np.savetxt("solved.dat", np.transpose([x, y, vx, vy, E, J, C, r]), header = "x \
        y      vx      vy      E,      J,     C,     r", fmt='%.5f')




    #Code for labelling second and third Lagrange points as well as the positions of each star.
    labels, xs, ys = ['L2', 'L3', 'M1', 'M2', 'B'], [L2 , L3, -mu, mu1, 0], [0,0,0,0,0]
    for i, label in enumerate(labels):
        xval=xs[i]
        yval=ys[i]
        plt.scatter(xval,yval,marker = 'x', color = 'red', s=2.0) 
        plt.text(xval+0.02*a, yval,label,fontsize=12)

    #Code for plotting trajectory and saving figure
    plt.plot(x,y, linewidth=0.5, color='C0')
    plt.title("q={0:.3},".format(q) + " $r_{0}$" + " = ({0:.2f},{1:.2f}),".format(float(xoffset), E0[1]) + " $v_{0}$" + " = ({0:.2f},{1:.2f})".format(E0[2], E0[3]), fontsize='15' )
    plt.xlabel('x/a', fontsize='15')
    plt.ylabel('y/a', fontsize='15', labelpad=-3)
    ax=plt.gca()
    ax.tick_params(axis="x", labelsize=12)
    ax.tick_params(axis="y", labelsize=12)
    plt.Axes.get_xscale
    plt.savefig(f"TP {q}, ({float(xoffset)},{E0[1]}), ({E0[2]},{E0[3]}).png", dpi=300)
    plt.show()


#Setting q value/values and running the code
q_vals = [1.0] #np.linspace(0.1, 1.0, 10)
for q in q_vals:
    M2 = 1
    M1 = M2/q
    mu = M2/(M1+M2)
    mu1=M1/(M1+M2)
    a=(mu1+mu)
    run_code(q)
